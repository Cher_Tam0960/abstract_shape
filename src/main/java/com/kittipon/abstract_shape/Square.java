/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.abstract_shape;

/**
 *
 * @author kitti
 */
public class Square extends Shape{
    private double side;
    public Square(double side) {
        super("Side");
        this.side = side;
    }

    @Override
    public double calArea() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Square : " + calArea();
    }
    
}
