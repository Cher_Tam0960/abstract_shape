/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.abstract_shape;

/**
 *
 * @author kitti
 */
public class Circle extends Shape {

    private double r;
    private double pi = 22.0 / 7;

    public Circle(double r) {
        super("Circle");
        this.r = r;
    }

    @Override
    public double calArea() {
        return pi * r * r;
    }

    @Override
    public String toString() {
        return "Circle : " + calArea();
    }

}
