/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.abstract_shape;

/**
 *
 * @author kitti
 */
public class Test {

    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(3, 2);
        System.out.println("r1 " + r1);
        Rectangle r2 = new Rectangle(4, 3);
        System.out.println("r2 " + r2);
        nextLine();
        Square s1 = new Square(4.0);
        System.out.println("s1 " + s1);
        Square s2 = new Square(2);
        System.out.println("s2 " + s2);
        nextLine();
        Circle c1 = new Circle(5.2);
        System.out.println("c1 " + c1);
        Circle c2 = new Circle(4.5);
        System.out.println("c2 " + c2);
    }

    private static void nextLine() {
        System.out.println("");
    }

}
